from ipyleaflet import Map, DrawControl, Marker, Rectangle
from sentinelhub import BBox, CRS, WmsRequest, DataSource, CustomUrlParam, SHConfig
import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy
from PIL import Image
import rasterio
from secret import PYTHON_INSTANCE_ID

resolution = 15
width = 5000
height = 5000

min_x = 12596572
min_y = -329460
max_x = min_x + resolution * width
max_y = min_y + resolution * height

POC_AREA = BBox(bbox=[min_x, min_y, max_x, max_y], crs=CRS.POP_WEB)

config = SHConfig()
config.instance_id = PYTHON_INSTANCE_ID


def create_request(time_range, bbox, resolution, layer):
    size_x = abs(int((bbox.max_x - bbox.min_x) / resolution))
    size_y = abs(int((bbox.max_y - bbox.min_y) / resolution))

    return WmsRequest(
        data_folder='data_dir',
        layer=layer,
        bbox=bbox,
        width=size_x,
        height=size_y,
        time=time_range,
        config=config,
        data_source=DataSource.SENTINEL1_IW,
        custom_url_params={
            CustomUrlParam.ATMFILTER: 'ATMCOR',
            CustomUrlParam.TRANSPARENT: True,
            CustomUrlParam.SHOWLOGO: False
        }
    )


def merge_images(imgs):
    """Since the original Radar data is tiled, a BBox might intersect with multiple tiles.
    This results in receiving multiple images with partly empty data.
    This function is meant to find and replace missing data from one img with the next one
    The result is 1 img where all data is combined.

    Use with care when merging images that differ a lot in their sensing time
    """
    image = deepcopy(imgs[0])
    band_channels = image.shape[2] - 1
    og_shape = image[:, :, 0].shape
    flat_bands = [image[:, :, i].flatten() for i in range(band_channels)]
    for img in imgs[1:len(imgs)]:
        # Find indexes which have data in this image
        indexes = np.where(img[:, :, band_channels].flatten() != 0)
        for i in range(band_channels):
            flat_bands[i][indexes] = img[:, :, i].flatten()[indexes]

    # Reshape all bands to make image complete
    for i in range(band_channels):
        image[:, :, i] = flat_bands[i].reshape(og_shape)
    return image[:, :, 0:band_channels]


def export_image(nparray, folder, filename):
    im = Image.fromarray(nparray)
    folder = folder if folder.endswith("/") else folder + "/"
    im.save(folder + filename + ".jpeg")


def export_geotiff(img, bbox, folder, filename):
    width = img.shape[1]
    height = img.shape[0]
    bands = img.shape[2]
    dst_transform = rasterio.transform.from_bounds(*bbox.lower_left, *bbox.upper_right,
                                                   width=width, height=height)
    dst_crs = {'init': CRS.ogc_string(bbox.crs)}
    folder = folder if folder.endswith("/") else folder + "/"
    with rasterio.open("{}{}.tif".format(folder, filename), 'w', driver='GTiff',
                       width=width, height=height,
                       count=bands, dtype=np.uint8, nodata=0,
                       transform=dst_transform, crs=dst_crs) as dst:
        for b in range(bands):
            dst.write_band(b + 1, img[:, :, b].astype(np.uint8))


def plot_image(image, factor=1):
    plt.subplots(nrows=1, ncols=1, figsize=(20, 20))
    if np.issubdtype(image.dtype, np.floating):
        plt.imshow(np.minimum(image * factor, 1))
    else:
        plt.imshow(image)


class WMSRequest:

    def __init__(self, time_range, bbox=POC_AREA, resolution=resolution, layer="TRUE-COLOR-S1-IW", data_dir="data_dir"):
        self.resolution = resolution
        self.bbox = bbox.transform(CRS.POP_WEB)
        self.layer = layer
        self.request = create_request(time_range, bbox, resolution, layer)
        self._dates = None
        self._data = None
        self._images = None

    @property
    def dates(self):
        if not self._dates:
            self._dates = [d.strftime("%Y-%m-%d") for d in self.request.get_dates()]
        return self._dates

    @property
    def data(self):
        if not self._data:
            self._data = self.request.get_data()
        return self._data

    @property
    def images(self):
        if self._images is None:
            images = []
            for date in set(self.dates):
                images.append(WMSImage(date, self.bbox, self.resolution, self.layer))
            self._images = images
        return self._images

    def export_images(self, folder=".", tif=False):
        for image in self.images:
            image.export_image(folder, tif)


class WMSImage:

    def __init__(self, date, bbox, resolution, layer):
        self.date = date
        self.bbox = bbox
        self.resolution = resolution
        self.layer = layer
        self._request = None
        self._image = None

    @property
    def request(self):
        if self._request is None:
            self._request = create_request((self.date, self.date), self.bbox, self.resolution, self.layer)
        return self._request

    @property
    def image(self):
        if self._image is None:
            self._image = merge_images(self.request.get_data())
        return self._image

    def export_image(self, folder=".", tif=False):
        if tif:
            export_geotiff(self.image, self.bbox, folder, self.date)
        else:
            export_image(self.image, folder, self.date)
        print("Exported image: " + self.date)

    def plot(self):
        plot_image(self.image)


class BBoxSelector:
    def __init__(self, bbox, zoom=8, resolution=10):
        center = (bbox.min_y + bbox.max_y) / 2, (bbox.min_x + bbox.max_x) / 2
        self.map = Map(center=center, zoom=zoom, scroll_wheel_zoom=True)

        self.resolution = resolution

        control = DrawControl()

        control.rectangle = {
            "shapeOptions": {
                "fillColor": "#fabd14",
                "color": "#fa6814",
                "fillOpacity": 0.2
            }
        }

        # Disable the rest of draw options
        control.polyline = {}
        control.circle = {}
        control.circlemarker = {}
        control.polygon = {}
        control.edit = False
        control.remove = False

        control.on_draw(self._handle_draw)

        self.map.add_control(control)

        self.bbox = None
        self.size = None
        self.rectangle = None
        self.add_rectangle(bbox.min_x, bbox.min_y, bbox.max_x, bbox.max_y)

    def add_rectangle(self, min_x, min_y, max_x, max_y):
        if self.rectangle:
            self.map.remove_layer(self.rectangle)

        self.rectangle = Rectangle(
            bounds=((min_y, min_x), (max_y, max_x)),
            color="#fa6814",
            fill=True,
            fill_color="#fabd14",
            fill_opacity=0.2,
            weight=1
        )

        self.map.add_layer(self.rectangle)

        self.bbox = BBox(((min_x, min_y), (max_x, max_y)), CRS.WGS84).transform(CRS.POP_WEB)

        # self.out.append_display_data((min_x, min_y, max_x, max_y))

        size_x = abs(int((self.bbox.max_x - self.bbox.min_x) / self.resolution))
        size_y = abs(int((self.bbox.max_y - self.bbox.min_y) / self.resolution))

        self.size = size_x, size_y

    def _handle_draw(self, control, action, geo_json):
        control.clear_rectangles()

        bbox_geom = geo_json['geometry']['coordinates'][0]

        min_x, min_y = bbox_geom[0]
        max_x, max_y = bbox_geom[2]

        self.add_rectangle(min_x, min_y, max_x, max_y)

    def show(self):
        return self.map
        # return self.vbox
