from utils import WMSRequest, WMSImage

search_time_range = ('2015-10-01T00:00:00', '2020-02-28T23:59:59')
request = WMSRequest(search_time_range)
print("Found dates: {}".format(len(set(request.dates))))
for image in request.images:
    image.export_image(folder="../data/raw/", tif=True)
