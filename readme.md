# Thesis code for Joep Klein Teeselink, 2031104

# Environment
Using the following command you can create a docker-based jupyter environment from which you can run all code:
`docker run --gpus all -it -p 8888:8888 -v /path/to/local/repo/ews:/tf --rm tensorflow/tensorflow:latest-gpu-jupyter`
After entering a notebook, make sure you install required packages by running `!pip install -r "../requirements.txt"` inside the notebook

# SarVision Label data
* Place them in `data` in the following structure: 
    * `data\basemap` should contain files per location i.e. `003_1_Basemap.tif`
    * `data\deforestation` should contain a folder per location `003_1`, `003_2` etc.
        * Within the location folder label images per date are expected.

# Getting Google Earth SAR images
* `googleearth\googleearth_data.ipynb` is a notebook that contains info on how to download from Google Earth Engine, store in GCS and download to local files
* `googleearth\download.py` contains some experimental Sentinel-1 / Sentinel-2 band stacking

# Creating Patched datasets
* `googleearth\tf_records_generator.py`
* `googleearth\googleearth_data.ipynb` Contains some implementation of generating patches

Patches are stored in `googleearth\tfrecords`

# Machine learning models
* `models\center_pixel_prediction.ipynb` Contains all code to train, evaluate & visualize models based on center-pixel based
* `models\train_unet.py` Code to train Deep U-Net (hasn't been ran in a while idk if it still works with current data structures)