import os, io
import math
import zipfile
from urllib.request import urlopen
import numpy as np
import rasterio
import rasterio.warp
import rasterio.windows
import rasterio.plot
from rasterio.coords import BoundingBox
from glob import glob
import traceback
from rasterio.errors import RasterioIOError
import random
import tensorflow as tf

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)

"""
SARVISION ORIGINAL LABELS
# 1 OR 100: Forest
# 2 <-> 99: Deforestation
# 101 - 253: Degredation

BASEMAP ORIGINAL LABELS
# 0 Unclassified/nodata
# 1 Forest
# 2 Peat Forest
# 3 Peat Forest Degraded
# 4 Mangrove
# 5 Swamp Forest
# 6 Degraded Forest
# 7 Degraded Vegetation
# 8 Non-forest
# 9 Oilpalm Plantations
# 10 Other Plantations
# 11 Forest Plantations
# 12 Water
# 13 Slopes/Nodata
"""

# This needs to be relative to the python file that imports this module
DEFORESTATION_FOLDER = "../data/deforestation"
BASEMAP_FOLDER = "../data/basemap"
DTYPE = np.float32


def getUniqueFile(fileName):
    index = 0
    baseName = fileName.split(".")[0]
    extension = fileName.split(".")[1]
    while os.path.isfile(fileName):
        index += 1
        fileName = baseName + "-{}.{}".format(index, extension)
    return fileName


def get_date(file):
    return [split for split in file.split("/") if len(split) == 10][0]


def get_file_name(f):
    return "/" + f.split("/")[-1]


def getDeforestationFile(location, date):
    files = glob("{}/{}/*.tif".format(DEFORESTATION_FOLDER, location))
    for f in files:
        if date.replace("_", "") in f:
            return f
    return None


def getBasemapFile(location):
    files = glob("{}/{}_Basemap.tif".format(BASEMAP_FOLDER, location))
    return files[0]


def get_batched_dataset(dataset, batch_sz=100, buffer_size=3000):
    # dataset = dataset.cache()  # This dataset fits in RAM
    dataset = dataset.repeat()
    dataset = dataset.shuffle(buffer_size=buffer_size)
    dataset = dataset.batch(batch_sz, drop_remainder=True)
    return dataset


def read_tfrecord_shape(serialized_record):
    """Simply returns the records (doesn't transform)
    Used to determmine the shape of the img
    """
    feature_description = {
        'image': tf.io.FixedLenFeature([], tf.string),
        'labels': tf.io.FixedLenFeature([], tf.string),
        'width': tf.io.FixedLenFeature([], tf.int64),
        'height': tf.io.FixedLenFeature([], tf.int64),
        'n_bands': tf.io.FixedLenFeature([], tf.int64),
        'n_classes': tf.io.FixedLenFeature([], tf.int64)
    }
    record = tf.io.parse_single_example(serialized_record, feature_description)
    return record


def read_tfrecord(serialized_record):
    feature_description = {
        'image': tf.io.FixedLenFeature([], tf.string),
        'labels': tf.io.FixedLenFeature([], tf.string),
        'width': tf.io.FixedLenFeature([], tf.int64),
        'height': tf.io.FixedLenFeature([], tf.int64),
        'n_bands': tf.io.FixedLenFeature([], tf.int64),
        'n_classes': tf.io.FixedLenFeature([], tf.int64)
    }
    record = tf.io.parse_single_example(serialized_record, feature_description)

    image = tf.io.parse_tensor(record['image'], out_type=DTYPE)
    image_shape = [record['width'], record['height'], record['n_bands']]
    image = tf.reshape(image, image_shape)
    image = tf.where(tf.math.is_nan(image), 0.0, image)

    labels = tf.io.parse_tensor(record['labels'], out_type=DTYPE)
    labels_shape = [record['width'], record['height'], record['n_classes']]
    labels = tf.reshape(labels, labels_shape)
    return image, labels


def read_tfrecord_shape_center_pixel(serialized_record):
    """Simply returns the records (doesn't transform)
    Used to determmine the shape of the img for center pixel dataset
    """
    feature_description = {
        'image': tf.io.FixedLenFeature([], tf.string),
        'labels': tf.io.FixedLenFeature([], tf.int64),
        'width': tf.io.FixedLenFeature([], tf.int64),
        'height': tf.io.FixedLenFeature([], tf.int64),
        'n_bands': tf.io.FixedLenFeature([], tf.int64),
        'n_classes': tf.io.FixedLenFeature([], tf.int64)
    }
    record = tf.io.parse_single_example(serialized_record, feature_description)
    return record


def read_tfrecord_center_pixel(serialized_record):
    feature_description = {
        'image': tf.io.FixedLenFeature([], tf.string),
        'labels': tf.io.FixedLenFeature([], tf.int64),
        'width': tf.io.FixedLenFeature([], tf.int64),
        'height': tf.io.FixedLenFeature([], tf.int64),
        'n_bands': tf.io.FixedLenFeature([], tf.int64),
        'n_classes': tf.io.FixedLenFeature([], tf.int64)
    }
    record = tf.io.parse_single_example(serialized_record, feature_description)

    image = tf.io.parse_tensor(record['image'], out_type=DTYPE)
    image_shape = [record['width'], record['height'], record['n_bands']]
    image = tf.reshape(image, image_shape)
    image = tf.where(tf.math.is_nan(image), 0.0, image)
    labels = record['labels']
    return image, labels


def random_transform(img, mask=np.array([[[0]]])):
    random_transformation = np.random.randint(1, 8)
    if random_transformation == 1:  # reverse first dimension
        img = img[::-1, :, :]
        mask = mask[::-1, :, :]
    elif random_transformation == 2:  # reverse second dimension
        img = img[:, ::-1, :]
        mask = mask[:, ::-1, :]
    elif random_transformation == 3:  # transpose(interchange) first and second dimensions
        img = img.transpose([1, 0, 2])
        mask = mask.transpose([1, 0, 2])
    elif random_transformation == 4:
        img = np.rot90(img, 1)
        mask = np.rot90(mask, 1)
    elif random_transformation == 5:
        img = np.rot90(img, 2)
        mask = np.rot90(mask, 2)
    elif random_transformation == 6:
        img = np.rot90(img, 3)
        mask = np.rot90(mask, 3)
    else:
        pass
    return img, mask


def get_rand_patch(img, mask, sz=160, transform=True, add_vv_vh=False, dtype=np.float32):
    """
    :param img: ndarray with shape (x_sz, y_sz, num_channels)
    :param mask: binary ndarray with shape (x_sz, y_sz, num_classes)
    :param sz: size of random patch
    :return: patch with shape (sz, sz, num_channels)
    """
    assert len(img.shape) == 3 and img.shape[0] > sz and img.shape[1] > sz and img.shape[0:2] == mask.shape[0:2]
    xc = random.randint(0, img.shape[0] - sz)
    yc = random.randint(0, img.shape[1] - sz)
    patch_img = img[xc:(xc + sz), yc:(yc + sz)]
    patch_mask = mask[xc:(xc + sz), yc:(yc + sz)]

    # Apply / 255 transformation on it since we need to normalize color channels for NN
    # return np.array(patch_img / 255, dtype=np.float32), patch_mask
    # For sentinel-1 the data range for the regular channels is -50 to 1, and 0 to 90 for the 3rd (angle) channel
    patch_img[:, :, [0, 1]] = (patch_img[:, :, [0, 1]] - - 30) / (40.29 - -30)
    patch_img[:, :, 2] = (patch_img[:, :, 2] - 0) / (90 - 0)

    ## add VV/VH band to patch
    if add_vv_vh:
        patch_img = np.dstack((patch_img, patch_img[:, :, 0] / patch_img[:, :, 1]))
        max = np.nanmax(patch_img[:, :, -1])
        min = np.nanmin(patch_img[:, :, -1])
        patch_img[:, :, -1] = (patch_img[:, :, -1] - min) / (max - min)

    # Apply some random transformations
    if transform:
        patch_img, patch_mask = random_transform(patch_img, patch_mask)

    return np.array(patch_img, dtype=dtype), np.array(patch_mask, dtype=dtype)


def get_rand_patches_1pp(img, mask, sz=301, n_patches=10, transform=True, add_vv_vh=False, dtype=np.float32):
    """
    :param img: ndarray with shape (x_sz, y_sz, num_channels)
    :param mask: binary ndarray with shape (x_sz, y_sz, num_classes)
    :param sz: size of random patch
    :return: patch with shape (sz, sz, num_channels)
    """
    assert sz % 2 == 1 and len(img.shape) == 3 and img.shape[1] > sz
    assert img.shape[0] > sz and img.shape[0:2] == mask.shape[0:2]
    assert n_patches % 2 == 0

    # Apply / 255 transformation on it since we need to normalize color channels for NN
    # return np.array(patch_img / 255, dtype=np.float32), patch_mask
    # For sentinel-1 the data range for the regular channels is -50 to 1, and 0 to 90 for the 3rd (angle) channel
    img[:, :, [0, 1]] = (img[:, :, [0, 1]] - - 30) / (40.29 - -30)
    img[:, :, 2] = (img[:, :, 2] - 0) / (90 - 0)

    ## add VV/VH band to patch
    if add_vv_vh:
        patch_img = np.dstack((img, img[:, :, 0] / img[:, :, 1]))
        max = np.nanmax(img[:, :, -1])
        min = np.nanmin(img[:, :, -1])
        img[:, :, -1] = (img[:, :, -1] - min) / (max - min)

    # Extract feasible area for obtaining center pixels
    # _________________________
    # |                        |
    # |    ________________    |
    # |   |                |   |
    # |   |  feasible mask |   |
    # |   |________________|   |
    # |                        |
    # |________________________|

    padding = math.ceil(sz / 2)
    feasible_mask = mask[padding:mask.shape[0] - padding, padding:mask.shape[1] - padding, :]
    y_trues, x_trues, _ = np.where(feasible_mask == 1)
    y_false, x_false, _ = np.where(feasible_mask == 0)

    def get_patches_for_center(img, ys, xs, n_patches):
        indices = random.choices(range(0, len(ys)), k=n_patches)
        x = []
        y = []
        for i in range(n_patches):
            indice = indices[i]
            yc = ys[indice]
            xc = xs[indice]
            patch = img[yc:yc + sz, xc:xc + sz, :]
            if transform:
                patch, _ = random_transform(patch)
            x.append(patch)
            y.append(feasible_mask[yc, xc, 0])
        return x, y

    imgs, labels = get_patches_for_center(img, y_trues, x_trues, (int)(n_patches / 2))
    imgs_false, labels_false = get_patches_for_center(img, y_false, x_false, (int)(n_patches / 2))
    imgs.extend(imgs_false)
    labels.extend(labels_false)
    return np.array(imgs, dtype=dtype), np.array(labels, dtype=dtype)


def get_rand_patches_1pp_t1(img, mask, maskt1, sz=301,
                            n_patches=10, transform=True,
                            add_vv_vh=False, dtype=np.float32):
    """
    :param img: ndarray with shape (x_sz, y_sz, num_channels)
    :param mask: binary ndarray with shape (x_sz, y_sz, num_classes)
    :param maskt1: binary ndarray with shape (x_sz, y_sz, num_classes)
    :param sz: size of random patch
    :return: patch with shape (sz, sz, num_channels)
    """
    assert sz % 2 == 1 and len(img.shape) == 3 and img.shape[1] > sz
    assert img.shape[0] > sz and img.shape[0:2] == mask.shape[0:2]
    assert mask.shape[0:2] == maskt1.shape[0:2]
    assert n_patches % 2 == 0

    # Apply / 255 transformation on it since we need to normalize color channels for NN
    # return np.array(patch_img / 255, dtype=np.float32), patch_mask
    # For sentinel-1 the data range for the regular channels is -50 to 1, and 0 to 90 for the 3rd (angle) channel
    img[:, :, [0, 1]] = (img[:, :, [0, 1]] - - 30) / (40.29 - -30)
    img[:, :, 2] = (img[:, :, 2] - 0) / (90 - 0)

    ## add VV/VH band to patch
    if add_vv_vh:
        patch_img = np.dstack((img, img[:, :, 0] / img[:, :, 1]))
        max = np.nanmax(img[:, :, -1])
        min = np.nanmin(img[:, :, -1])
        img[:, :, -1] = (img[:, :, -1] - min) / (max - min)

    # Extract feasible area for obtaining center pixels
    # _________________________
    # |                        |
    # |    ________________    |
    # |   |                |   |
    # |   |  feasible mask |   |
    # |   |________________|   |
    # |                        |
    # |________________________|

    padding = math.ceil(sz / 2)
    feasible_mask = mask[padding:mask.shape[0] - padding, padding:mask.shape[1] - padding, :]
    feasible_maskt1 = maskt1[padding:maskt1.shape[0] - padding, padding:maskt1.shape[1] - padding, :]

    merged = np.concatenate((feasible_mask, feasible_maskt1), axis=2)
    # concat them so the 3rd axis is 2 channels (first mask & second mask)
    # [0] - [1] == -1 -> the pixel was non-deforest and is now deforest
    indexes_deforest = np.array(np.where(merged[:, :, 0] - merged[:, :, 1] == -1)).transpose([1, 0])
    indexes_nodeforest = np.array(np.where(merged[:, :, 0] + merged[:, :, 1] == 0)).transpose([1, 0])

    assert len(indexes_deforest) > n_patches / 2

    def get_patches_for_index(img, indexes, n_patches):
        random_samples = random.choices(range(0, len(indexes)), k=n_patches)
        patches = []
        for i in range(n_patches):
            sample_index = random_samples[i]
            yc = indexes[sample_index][0]
            xc = indexes[sample_index][1]
            # We dont have to subtract the padding since we are getting indices from the inner box
            patch = img[yc:yc + sz, xc:xc + sz, :]
            if transform:
                patch, _ = random_transform(patch)
            patches.append(patch)
        return patches

    imgs = get_patches_for_index(img, indexes_deforest, (int)(n_patches / 2))
    imgs_false = get_patches_for_index(img, indexes_nodeforest, (int)(n_patches / 2))

    imgs.extend(imgs_false)
    labels = np.ones((int)(n_patches / 2))
    labels = np.append(labels, np.zeros((int)(n_patches / 2)))
    return np.array(imgs, dtype=dtype), np.array(labels, dtype=dtype)


def load_numpy(file, single_class=None):
    try:
        x = rasterio.open(file).read().transpose([1, 2, 0])
        y = getLabelsForTIF(file).transpose([1, 2, 0])
    except (RasterioIOError, AssertionError, TypeError) as e:
        print("Error getting data of file: {}".format(file))
        print(e)
        traceback.print_exc()
        return None, None

    # N_BANDS is index of labels/mask
    if single_class is not None:
        y = y[:, :, single_class]

    x = x[:y.shape[0], :y.shape[1], :]
    return x, y


def getLabelsForTIF(file):
    """
    Obtains the labels for given TIF satellite image
    Assumes that
    """
    date = os.path.split(os.path.split(file)[0])[1]
    location = os.path.split(file)[0].split("/")[-2]
    # print("Getting labels for: {}, with date: {}".format(file, date))
    deforestationFile = getDeforestationFile(location, date)

    tifFile = rasterio.open(file)

    deforestation = get_windowed_band(tifFile, rasterio.open(deforestationFile))
    basemap = get_windowed_band(tifFile, rasterio.open(getBasemapFile(location)))
    return get_labels(deforestation, basemap)


def get_labels(deforest, basemap, n_classes=6):
    """Extracts labels from deforest and basemap window (assumes windows are of equal size)
    Creates one-hot-encoding with following index-label mapping
        0: Forest
        1: Deforestation
        2: Degredation
        3: Non-forest
        4: Plantation
        5: Water
    """
    shape = deforest.shape
    assert shape[0] == basemap.shape[0] and shape[1] == basemap.shape[1]
    flat_def = deforest.flatten()
    flat_base = basemap.flatten()

    determinators = {  ## Determines for each class which indexes will be labeled 1
        0: np.logical_or(flat_def == 1, flat_def == 100),
        1: np.logical_and(flat_def >= 2, flat_def <= 99),
        2: np.logical_and(flat_def >= 101, flat_def <= 253),
        3: np.logical_or(flat_def == 0, flat_base == 8),
        4: np.logical_or(flat_base == 9, flat_base == 10, flat_base == 11),
        5: np.where(flat_base == 12)
    }

    labels = np.zeros((n_classes, shape[0], shape[1]))
    for i in range(n_classes):
        flat_labels = np.zeros((shape[0], shape[1])).flatten()  # Create flat list of fitting size
        flat_labels[determinators[i]] = 1  # Set all indexes to one according to determinator
        labels[i] = flat_labels.reshape((shape[0], shape[1]))  # Push labels in hot-encoded list

    return np.array(labels, dtype=np.single)


def get_windowed_band(raw, deforestation):
    bounds = raw.bounds
    # Transform bounds of raw satellite images to deforestation crs
    bounds_tuple = rasterio.warp.transform_bounds(raw.crs, deforestation.crs,
                                                  bounds.left, bounds.bottom,
                                                  bounds.right, bounds.top,
                                                  densify_pts=21)
    warped_bounds = BoundingBox(bounds_tuple[0], bounds_tuple[1], bounds_tuple[2], bounds_tuple[3])
    # Create window applicable on deforestation
    window = rasterio.windows.from_bounds(warped_bounds.left, warped_bounds.bottom,
                                          warped_bounds.right, warped_bounds.top,
                                          transform=deforestation.transform, precision=None,
                                          height=raw.height, width=raw.width)
    return deforestation.read(1, window=window)


def create_folder(path):
    if not os.path.exists(path):
        os.makedirs(path)


def create_folders(path):
    create_folder(path[0:path.rindex("/")] if "." in path else path)


def downloadZIP(url):
    usock = urlopen(url)
    filebytes = io.BytesIO(usock.read())
    return zipfile.ZipFile(filebytes)


class Namespace(dict):
    """
    Class that allows easy access of object members (both for set / get)
    """

    def __init__(self, *args, **kwargs):
        super(Namespace, self).__init__(*args, **kwargs)
        for arg in args:
            if isinstance(arg, dict):
                for k, v in arg.iteritems():
                    self[k] = v

        if kwargs:
            for k, v in kwargs.iteritems():
                self[k] = v

    def __getattr__(self, attr):
        return self.get(attr)

    def __setattr__(self, key, value):
        self.__setitem__(key, value)

    def __setitem__(self, key, value):
        super(Namespace, self).__setitem__(key, value)
        self.__dict__.update({key: value})

    def __delattr__(self, item):
        self.__delitem__(item)

    def __delitem__(self, key):
        super(Namespace, self).__delitem__(key)
        del self.__dict__[key]


if __name__ == "__main__":
    dataset = tf.data.TFRecordDataset(glob("googleearth/tfrecords/single_pixel/*.tfrecords")).map(
        read_tfrecord_center_pixel)
    dataset = dataset.repeat()
    dataset = dataset.shuffle(buffer_size=10)
    dataset = dataset.batch(2, drop_remainder=True)
    for data, labels in dataset.take(1):
        print(data.shape)
        print("Labels: ", labels)
