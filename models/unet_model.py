# u-net model with up-convolution or up-sampling and weighted binary-crossentropy as loss func
import tensorflow as tf
from tensorflow.keras import metrics
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Conv2D, MaxPooling2D, UpSampling2D, concatenate, Conv2DTranspose, \
    BatchNormalization, Dropout
from tensorflow.keras.optimizers import Adam
import tensorflow.keras.backend as K


def weighted_binary_crossentropy_loss(options):
    """Multi-label loss (multiple possibilities per pixel (multi-dim))"""

    def weighted_binary_crossentropy(y_true, y_pred):
        class_loglosses = K.mean(K.binary_crossentropy(y_true, y_pred), axis=[0, 1, 2])
        constant = K.constant(options.class_weights)
        if options.down_scale:
            class_loglosses = tf.cast(class_loglosses, dtype='float16')
            constant = tf.cast(constant, dtype='float16')
        return K.sum(class_loglosses * constant)

    return weighted_binary_crossentropy


def conv_conv(filters, _input):
    conv = Conv2D(filters, (3, 3), activation='relu', padding='same')(_input)
    return Conv2D(filters, (3, 3), activation='relu', padding='same')(conv)


def create_down_layer(_input, filters, droprate):
    print("_input: ", _input)
    conv = conv_conv(filters, _input)
    print("conv: ", conv)
    pool = MaxPooling2D(pool_size=(2, 2))(conv)
    pool = Dropout(droprate)(pool)
    output = BatchNormalization()(pool)
    print("Producing: ", output)
    return conv, output


def create_up_layer(_input, sibling, filters, upconv, droprate):
    print("_input: ", _input)
    print("sibling: ", sibling)
    if upconv:
        up = concatenate([Conv2DTranspose(filters, (2, 2), strides=(2, 2), padding='same')(_input), sibling])
    else:
        up = concatenate([UpSampling2D(size=(2, 2))(_input), sibling])
    up = BatchNormalization()(up)
    conv = conv_conv(filters, up)
    output = Dropout(droprate)(conv)
    print("Producing: ", output)
    return output


def unet_model(options):
    """[im_sz] is the argument that decides which input size will be used for the images.
    It needs to be a multiply of 32"""

    growth_factor = options.growth_factor
    n_filters_start = options.n_filters_start
    layers = options.model_layers
    droprate = options.droprate
    upconv = options.upconv

    print("Creating U-net architecture::::")
    print("Image dimension: {}x{}".format(options.patch_size, options.patch_size))
    print("Bands: {}, Classes: {}".format(options.n_bands, options.n_classes))
    strategy = tf.distribute.MirroredStrategy()
    with strategy.scope():
        down_layers = {}
        up_layers = {}
        inputs = Input((options.patch_size, options.patch_size, options.n_bands))
        for i in range(layers):
            if i == 0:
                _input = inputs
                filters = n_filters_start
            else:
                _input = down_layers[i - 1]["output"]
                filters = down_layers[i - 1]["filters"]
                filters = filters * growth_factor
            ## Create down convolutional layer
            print("Creating DOWN layer: {} with: {} filters".format(i, filters))
            sibling, output = create_down_layer(_input, filters, droprate)
            down_layers[i] = {
                "sibling": sibling,
                "output": output,
                "filters": filters
            }

        for i in range(layers):
            if i == 0:
                _input = down_layers[layers - 1]["output"]
                filters = down_layers[layers - 1]["filters"]
            else:
                _input = up_layers[i - 1]["output"]
                filters = up_layers[i - 1]["filters"]
            sibling = down_layers[layers - i - 1]["sibling"]
            ## Create up convolutional layer
            filters = filters // growth_factor
            print("Creating UP layer: {} with: {} filters".format(i, filters))
            output = create_up_layer(_input, sibling, filters, upconv, droprate)
            up_layers[i] = {
                "output": output,
                "filters": filters
            }

        # Output layer
        last_up = up_layers[layers - 1]["output"]
        output = Conv2D(options.n_classes, kernel_size=(1, 1), strides=(1, 1), activation='sigmoid',
                        padding='valid')(last_up)

        model = Model(inputs=inputs, outputs=output)

        IoU = metrics.MeanIoU(num_classes=options.n_classes)
        options.metrics = [IoU, metrics.AUC()] if options.n_classes > 1 else [metrics.AUC()]
        if options.run_opts != None:
            model.compile(optimizer=Adam(), loss=options.loss, metrics=options.metrics,
                          options=options.run_opts)
        else:
            model.compile(optimizer=Adam(), loss=options.loss, metrics=options.metrics)

        return model
