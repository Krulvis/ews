from sklearn.neural_network import MLPClassifier
from sklearn.metrics import roc_auc_score, classification_report
from sklearn.model_selection import GridSearchCV
import argparse
import datetime
from glob import glob
import os, sys

parentdir = os.path.split(os.getcwd())[0]
sys.path.append(parentdir)

from utils import *

options = Namespace()


def get_single_batch_dataset(dataset, size=3000, shuffle=True):
    # dataset = dataset.cache()  # This dataset fits in RAM
    dataset = dataset.repeat()
    if shuffle:
        dataset = dataset.shuffle(buffer_size=size)
    dataset = dataset.batch(size)
    return dataset


def train_per_batch(clf, train_data, options, bands=options.n_bands):
    print("Training CLF using: {} samples.".format(options.train_size))
    steps = int(options.train_size / options.batch_size)
    for step, (x_train, y_train) in enumerate(train_data.take(steps)):
        print("Train set[{}]: {}".format(step, x_train.shape))
        print("Transforming data...")
        x_flat = x_train.numpy()[:, :, :, 0:bands].reshape(options.batch_size,
                                                           options.patch_size * options.patch_size * bands)
        x_flat[np.isnan(x_flat)] = np.nanmean(x_flat)
        y = y_train.numpy()
        print("Fitting model...")
        clf.fit(x_flat, y)
    return clf


def train(clf, train_data, options, bands=options.n_bands):
    print("Training CLF using: {} samples.".format(options.train_size))
    for x_train, y_train in train_data.take(1):
        shape = x_train.shape
        print("Train size: [{}, {}, {}]".format(shape[1], shape[2], bands))
        print("Transforming data...")
        x_flat = x_train.numpy()[:, :, :, 0:bands].reshape(options.train_size,
                                                           options.patch_size * options.patch_size * bands)
        x_flat[np.isnan(x_flat)] = np.nanmean(x_flat)
        y = y_train.numpy()
        print("Fitting model...")
        clf.fit(x_flat, y)
        del x_flat
    return clf


def validate(clf, val_data, options, bands=options.n_bands):
    print("Validating on: {} samples.".format(options.val_size))
    for x_test, y_test in val_data.take(1):
        x_flat = x_test.numpy()[:, :, :, 0:bands].reshape(options.val_size,
                                                          options.patch_size * options.patch_size * bands)
        x_flat[np.isnan(x_flat)] = np.nanmean(x_flat)
        y_test = y_test.numpy()
        preds = clf.predict(x_flat)
        auc = roc_auc_score(y_test, preds)
        print("Test AUC: {}".format(round(auc, 2)))
        cr = classification_report(y_test, preds)
        print(cr)
        return cr


if __name__ == "__main__":
    # 1 is forest, 2 is deforestation, 3 is degradation, 4 is non-forest, 5 plantation, 6 water

    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument("-d", "--data", required=True, help="Data folder to take data from")

    options["class_weights"] = [1]
    options["n_epochs"] = 150
    options["train_size"] = 3000
    options["val_size"] = 1000
    options.batch_size = 100
    options.dropout = 0.25
    options.l_r = 0.0001

    folder = "../googleearth/tfrecords/center_pixel_t4/003_1_2018_51ps_all_context/float32/"
    train_files = glob("{}**/train*.tfrecords".format(folder), recursive=True)
    test_files = glob("{}**/test*.tfrecords".format(folder), recursive=True)

    train_data_batched = get_batched_dataset(
        tf.data.TFRecordDataset(train_files).map(read_tfrecord_center_pixel),
        options.batch_size,
        options.train_size
    )

    train_data = get_single_batch_dataset(
        tf.data.TFRecordDataset(train_files).map(read_tfrecord_center_pixel),
        options.train_size,
        shuffle=True
    )

    # One batch validation data is the full set
    val_data = get_single_batch_dataset(
        tf.data.TFRecordDataset(test_files).map(read_tfrecord_center_pixel),
        size=options.val_size,
        shuffle=False
    )

    now = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M")

    output_file = open("./results/center_pixel_results_{}.txt".format(now), "w")

    for record in tf.data.TFRecordDataset(train_files).map(read_tfrecord_shape_center_pixel).take(1):
        options["patch_size"] = record["width"].numpy()
        options["n_classes"] = record["n_classes"].numpy()
        options["n_bands"] = record["n_bands"].numpy()
        print(options)
        output_file.write(str(options))

    ## Grid search params
    grid_values = {'hidden_layer_sizes': [(100,), (100, 50,), (50,), (50, 10,), (100, 50, 10,)],
                   'learning_rate_init': [0.1, 0.01, 0.001, 0.0001],
                   'batch_size': [25, 50, 100, 150, 200, 250],
                   'alpha': [1, 0.1, 0.01, 0.001, 0.0001, 0.00001]}

    gs = GridSearchCV(
        MLPClassifier(),
        param_grid=grid_values,
        scoring='roc_auc'
    )

    gs = train(gs, train_data, options, 3)
    cr = validate(gs, val_data, options, 3)
    print("Best score: ", gs.best_score_)
    print("Best params: ", gs.best_params_)
    output_file.write("Best score: {}".format(gs.best_score_))
    output_file.write("Best params: {}".format(gs.best_params_))
    output_file.write(cr)

    output_file.close()
