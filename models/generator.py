import tensorflow as tf
import os, sys
import random

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from utils import *


# This is old generator: Generates patches during training which means 4k new patches every epoch.
# Very slow

class Generator:

    def __init__(self, files, batch_size, N_BANDS=3, N_CLASSES=6, PATCH_SZ=160):
        self.file_index = 0
        self.n_bands = N_BANDS
        self.n_classes = N_CLASSES
        self.patch_sz = PATCH_SZ
        self.files = files
        self.batch_size = batch_size

        # Cache the file to generate 3 patches out of it
        self.current_file = None
        self.x = None
        self.y = None

    def get_generator(self):
        return tf.data.Dataset.from_generator(
            self.gen_data,
            output_types=(tf.float32, tf.float32),
            output_shapes=([self.batch_size, self.patch_sz, self.patch_sz, self.n_bands],
                           [self.batch_size, self.patch_sz, self.patch_sz, self.n_classes])
        )

    def get_file(self):
        if self.file_index >= len(self.files):
            self.file_index = 0
        return self.files[self.file_index]

    def get_x_y(self):
        if self.get_file() == self.current_file:
            return self.x, self.y
        else:
            self.x, self.y = load_numpy(self.get_file(), self.n_classes, self.n_bands)
        if self.x is None or self.y is None:  # Skip this file cuz its borky
            print("Skipping: {} cuz its borked".format(self.get_file()))
            self.file_index += 1
            return self.get_x_y()
        return self.x, self.y

    def get_batch(self):
        batch_x = []
        batch_y = []
        for i in range(self.batch_size):
            x, y = self.get_x_y()
            print("Generated: ", i)
            x, y = self.get_rand_patch(x, y)  # Trim to fit model dimensions
            batch_x.append(x)
            batch_y.append(y)
            if i % 3 == 0:  # 3 patches per files max
                self.file_index += 1
        return batch_x, batch_y

    def gen_data(self):  # should generate one batch
        while True:
            yield self.get_batch()

    def get_rand_patch(self, img, mask):
        return get_rand_patch(img, mask, self.patch_sz)
