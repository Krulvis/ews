import argparse
import datetime
import logging
import sys
from unet_model import *
import os.path
from tensorflow.keras.callbacks import CSVLogger
from tensorflow.keras.callbacks import TensorBoard
from tensorflow.keras.callbacks import ModelCheckpoint

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from utils import *

logging.basicConfig(filename='training_model.log', level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(name)s %(message)s')
logger = logging.getLogger(__name__)


def get_model(options):
    options.loss = weighted_binary_crossentropy_loss(options)
    if options.keras_model:
        print("Using keras-unet model")
        strategy = tf.distribute.MirroredStrategy()
        with strategy.scope():
            from keras_unet.models import custom_unet
            model = custom_unet(
                input_shape=(options.patch_size, options.patch_size, options.n_bands),
                use_batch_norm=False,
                num_classes=options.n_classes,
                filters=32,
                dropout=0.2,
                output_activation='sigmoid'
            )
            IoU = metrics.MeanIoU(num_classes=options.n_classes)
            options.metrics = [IoU, metrics.AUC()] if options.n_classes > 1 else [metrics.AUC()]
            model.compile(optimizer=Adam(),
                          loss=options.loss,
                          metrics=options.metrics
                          )
    else:
        print("Using Homebrew build model")
        model = unet_model(options)
    return model


now = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M")
weights_path = 'weights'
results_path = 'results'
create_folder(weights_path)
create_folder(results_path)

weights_path += '/{}.hdf5'.format(now)
results_path += '/{}.csv'.format(now)


def get_year(file):
    date = [part for part in file.split("/") if len(part) == 10][0]
    return int(date[0:4])


# trainIds = [str(i).zfill(2) for i in range(1, 25)]  # all availiable ids: from "01" to "24"

def main(args=None):
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument("-d", "--data", required=True, help="Data folder to take data from")
    parser.add_argument("-e", "--extension", required=False, default="npy",
                        help="What data file extensions are we using?")
    parser.add_argument("-m", "--max-input-files", required=False, help="Max input files", default=-1, type=int)
    parser.add_argument("-ts", "--train-size", required=False, help="Train set size", default=3000, type=int)
    parser.add_argument("-vs", "--val-size", required=False, help="Validation set size", default=1000, type=int)
    parser.add_argument("-bs", "--batch-size", required=False, help="Batch size", default=100, type=int)
    parser.add_argument("-ne", "--n-epochs", required=False, help="Epochs", default=150, type=int)
    parser.add_argument("-ml", "--model-layers", required=False, help="Convolutional layers in custom model", default=4,
                        type=int)
    parser.add_argument("-km", "--keras-model", required=False, help="Use Keras custom u-net model", default=False,
                        type=bool)
    parser.add_argument("-gf", "--growth-factor", required=False, help="Growth factor of model", default=2.0,
                        type=float)
    parser.add_argument("-ds", "--down-scale", required=False, help="Use float16 rather than float32", default=False,
                        type=bool)

    options = parser.parse_args(args)

    folder = options.data
    folder = folder if folder.endswith("/") else folder + "/"
    options.class_weights = [1]
    options.n_filters_start = 32
    options.upconv = True
    options.run_opts = None

    if options.down_scale:
        from tensorflow.keras.mixed_precision import experimental as mixed_precision
        policy = mixed_precision.Policy('mixed_float16')
        mixed_precision.set_policy(policy)

    for record in tf.data.TFRecordDataset(glob("{}**/train*.tfrecords".format(folder), recursive=True)).map(
            read_tfrecord_shape).take(1):
        options.patch_size = record["width"].numpy()
        options.n_classes = record["n_classes"].numpy()
        options.n_bands = record["n_bands"].numpy()

    print("Patch sz: {}, N_Classes: {}, N_Bands: {}".format(options.patch_size, options.n_classes, options.n_bands))

    train_dataset = tf.data.TFRecordDataset(glob("{}**/train*.tfrecords".format(folder), recursive=True)).map(
        read_tfrecord)
    train_data = get_batched_dataset(
        train_dataset,
        options.batch_size,
        3000
    )

    val_dataset = tf.data.TFRecordDataset(glob("{}**/test*.tfrecords".format(folder), recursive=True)).map(
        read_tfrecord)
    val_data = get_batched_dataset(
        val_dataset,
        options.batch_size,
        1000
    )

    def train_net(train_data, val_data):
        print("start train net")
        gpus = tf.config.experimental.list_physical_devices('GPU')
        if gpus:
            try:
                # Currently, memory growth needs to be the same across GPUs
                for gpu in gpus:
                    tf.config.experimental.set_memory_growth(gpu, True)
                logical_gpus = tf.config.experimental.list_logical_devices('GPU')
                print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
            except RuntimeError as e:
                # Memory growth must be set before GPUs have been initialized
                print(e)
        # run_opts = tf.compat.v1.RunOptions(report_tensor_allocations_upon_oom=True)
        model = get_model(options)
        if os.path.isfile(weights_path):
            print("Loading existing weights")
            model.load_weights(weights_path)
        # model_checkpoint = ModelCheckpoint(weights_path, monitor='val_loss', save_weights_only=True, save_best_only=True)
        # early_stopping = EarlyStopping(monitor='val_loss', min_delta=0, patience=10, verbose=1, mode='auto')
        # reduce_lr = ReduceLROnPlateau(monitor='loss', factor=0.1, patience=5, min_lr=0.00001)
        model_checkpoint = ModelCheckpoint(weights_path, monitor='val_loss', save_best_only=True)
        csv_logger = CSVLogger(results_path, append=True, separator=';')
        tensorboard = TensorBoard(log_dir='./tensorboard_unet/', write_graph=True, write_images=True)
        model.fit(train_data, validation_data=val_data, epochs=options.n_epochs,
                  verbose=2, shuffle=True, steps_per_epoch=3000 / options.batch_size,
                  validation_steps=1000 / options.batch_size,
                  # we have 3 patches per image
                  callbacks=[model_checkpoint, csv_logger, tensorboard]
                  )
        return model

    train_net(train_data, val_data)


if __name__ == '__main__':
    try:
        main()
    except MemoryError as e:
        logger.error(e)
