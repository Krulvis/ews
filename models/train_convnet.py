import argparse
import datetime
import logging
import os.path
import sys

from tensorflow.keras.callbacks import CSVLogger
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.callbacks import TensorBoard
from tensorflow.keras.layers import Conv2D, Dense, \
    Flatten, Dropout, Input, MaxPooling2D
from tensorflow.keras.optimizers import Adam

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from utils import *

logging.basicConfig(filename='training_conv_modell.log', level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(name)s %(message)s')
logger = logging.getLogger(__name__)


def get_lr_model(options):
    strategy = tf.distribute.MirroredStrategy()
    with strategy.scope():
        inputs = Input((options.patch_size, options.patch_size, options.n_bands))
        flat = Flatten()(inputs)
        outputs = Dense(options.n_classes, activation="sigmoid")(flat)

        model = tf.keras.Model(inputs=inputs, outputs=outputs)
        model.compile(optimizer=Adam(lr=0.00001), loss="binary_crossentropy",
                      metrics=['accuracy'])
        return model


def get_conv_model(options):
    strategy = tf.distribute.MirroredStrategy()
    with strategy.scope():
        inputs = Input((options.patch_size, options.patch_size, options.n_bands))

        conv = Conv2D(32, (3, 3), activation='relu', padding='same')(inputs)
        pool = MaxPooling2D(pool_size=(2, 2))(conv)

        conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(pool)
        pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

        conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(pool1)
        pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

        drop = Dropout(options.dropout)(pool2)
        flat = Flatten()(drop)
        dense = Dense(64, activation="relu")(flat)
        outputs = Dense(options.n_classes, activation="sigmoid")(dense)

        model = tf.keras.Model(inputs=inputs, outputs=outputs)
        # model = Sequential()
        # model.add(Conv2D(32, 3, padding="same", activation="relu",
        #                  input_shape=(options.patch_size, options.patch_size, options.n_bands)))
        # model.add(MaxPool2D())
        #
        # model.add(Conv2D(32, 3, padding="same", activation="relu"))
        # model.add(MaxPool2D())
        #
        # model.add(Conv2D(64, 3, padding="same", activation="relu"))
        # model.add(MaxPool2D())
        # model.add(Dropout(0.4))
        #
        # model.add(Flatten())
        # model.add(Dense(128, activation="relu"))
        # model.add(Dense(options.n_classes, activation="sigmoid"))
        #
        model.compile(optimizer=Adam(lr=0.000001), loss=tf.keras.losses.BinaryCrossentropy(),
                      metrics=['accuracy'])
        return model


def main(args=None):
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument("-d", "--data", required=True, help="Data folder to take data from")
    parser.add_argument("-ts", "--train-size", required=False, help="Train set size", default=3000, type=int)
    parser.add_argument("-vs", "--val-size", required=False, help="Validation set size", default=1000, type=int)
    parser.add_argument("-bs", "--batch-size", required=False, help="Batch size", default=100, type=int)
    parser.add_argument("-ne", "--n-epochs", required=False, help="Epochs", default=150, type=int)
    parser.add_argument("-do", "--dropout", required=False, help="Dropout rate", default=0.25, type=float)
    parser.add_argument("-ml", "--model-layers", required=False, help="Convolutional layers in custom model", default=4,
                        type=int)
    options = parser.parse_args(args)
    folder = options.data
    folder = folder if folder.endswith("/") else folder + "/"
    options.class_weights = [1]
    options.n_filters_start = 32
    options.upconv = True
    options.run_opts = None

    now = datetime.datetime.now().strftime("%Y-%m-%d_%H:%M")
    weights_path = 'weights'
    results_path = 'results'
    create_folder(weights_path)
    create_folder(results_path)

    weights_path += '/{}.hdf5'.format(now)
    results_path += '/{}.csv'.format(now)

    for record in tf.data.TFRecordDataset(glob("{}**/train*.tfrecords".format(folder), recursive=True)).map(
            read_tfrecord_shape_center_pixel).take(1):
        options.patch_size = record["width"].numpy()
        options.n_classes = record["n_classes"].numpy()
        options.n_bands = record["n_bands"].numpy()

    print("Patch sz: {}, N_Classes: {}, N_Bands: {}".format(options.patch_size, options.n_classes, options.n_bands))

    train_dataset = tf.data.TFRecordDataset(glob("{}**/train*.tfrecords".format(folder), recursive=True)).map(
        read_tfrecord_center_pixel)
    train_data = get_batched_dataset(
        train_dataset,
        options.batch_size,
        options.train_size
    )

    val_dataset = tf.data.TFRecordDataset(glob("{}**/test*.tfrecords".format(folder), recursive=True)).map(
        read_tfrecord_center_pixel)
    val_data = get_batched_dataset(
        val_dataset,
        options.batch_size,
        options.val_size
    )

    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            # Currently, memory growth needs to be the same across GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
            logical_gpus = tf.config.experimental.list_logical_devices('GPU')
            print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            print(e)
    model = get_lr_model(options)

    model_checkpoint = ModelCheckpoint(weights_path, monitor='val_loss', save_best_only=True)
    csv_logger = CSVLogger(results_path, append=True, separator=';')
    tensorboard = TensorBoard(log_dir='./tensorboard_unet/', write_graph=True, write_images=True)
    model.fit(train_data, validation_data=val_data, epochs=options.n_epochs,
              verbose=2, shuffle=True, steps_per_epoch=options.train_size / options.batch_size,
              validation_steps=options.val_size / options.batch_size,
              # we have 3 patches per image
              callbacks=[model_checkpoint, csv_logger, tensorboard]
              )


if __name__ == '__main__':
    try:
        main()
    except MemoryError as e:
        logger.error(e)
