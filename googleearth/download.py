import argparse
import json
import logging
import os
import sys
import ee
import secret
import time
from datetime import datetime, timedelta

logging.basicConfig(filename='ee_download.log', level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(name)s %(message)s')
logger = logging.getLogger(__name__)

ee.Initialize(ee.ServiceAccountCredentials(secret.EE_ACCOUNT, secret.EE_PRIVATE_KEY_FILE))

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from utils import *


def maskImage(image):
    edge = image.lt(-30.0)
    maskedImage = image.mask().And(edge.Not())
    return image.updateMask(maskedImage)



def getDeforestationFile(date):
    """
    :param date: used to find the fitting SarVision labels
    :return: first deforestation (SarVision labels) file that matches the date
    """
    files = glob("../data/deforestation/*.tif")
    for f in files:
        if date.replace("-", "") in f:
            return f
    return None


def getBasemapFile():
    """
    :returns basemap file according to my data structure
    """
    files = glob("../data/basemap/003_1_Basemap.tif")
    return files[0]


def getUniqueFile(fileName):
    """Adds number to file if it already exist to make sure it doesn't overwrite something else"""
    index = 0
    baseName = fileName.split(".")[0]
    extension = fileName.split(".")[1]
    while os.path.isfile(fileName):
        index += 1
        fileName = baseName + "-{}.{}".format(index, extension)
    return fileName



def exportTIF(image, region, file, date, name):
    task = ee.batch.Export.image.toCloudStorage(
        image=image,
        description='Sentinel1-2Combination',
        bucket='ews-data',
        fileNamePrefix='data/{}/{}/{}'.format(file, date, name),
        scale=15,
        region=region
    )
    task.start()
    return task


def maskS2clouds(image):
    """Mask the clouds out of the image"""
    qa = image.select('QA60')
    # Bits 10 and 11 are clouds and cirrus, respectively.
    cloudBitMask = 1 << 10
    cirrusBitMask = 1 << 11

    # Both flags should be set to zero, indicating clear conditions.
    mask = qa.bitwiseAnd(cloudBitMask).eq(0).And(qa.bitwiseAnd(cirrusBitMask).eq(0))

    return image.updateMask(mask).divide(10000)


def createCombinedImage(boundsFilter, timeFilter):
    """Stacks Sentinel-1 & Sentinel-2 image for given filters"""
    sentinel1 = ee.ImageCollection('COPERNICUS/S1_GRD') \
        .filter(boundsFilter) \
        .filter(timeFilter) \
        .filter(ee.Filter.eq('instrumentMode', 'IW')) \
        .filter(ee.Filter.listContains('transmitterReceiverPolarisation', 'VH')) \
        .map(maskImage) \
        .select(["VV", "VH"]).mean()

    sentinel2 = ee.ImageCollection('COPERNICUS/S2') \
        .filter(boundsFilter) \
        .filter(timeFilter) \
        .map(maskS2clouds) \
        .select(['B4', 'B3', 'B2']).mean()

    image = sentinel2.addBands(sentinel1)
    return image


def getRegions(file):
    regions = []
    with open(file) as f:
        data = json.loads(f.read())
        for feat in data["features"]:
            regions.append(ee.Geometry(feat["geometry"]))
        return regions


def mapTimes(img):
    return ee.Image(img).date().format()


def getSentinel2DatesForRegion(region):
    """Since there is cloud-coverage on Sentinel-2 images, we first need to check which dates have proper images
    :returns list of dates where there is only 25 % cloud coverage on Sentinel-2 image
    """
    boundsFilter = ee.Filter.geometry(ee.Geometry(region))
    collection = ee.ImageCollection('COPERNICUS/S2') \
        .filter(boundsFilter) \
        .filter(ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', 25))
    return ee.List(collection.toList(collection.size())).map(mapTimes).getInfo()


def downloadGCS():
    """Download all files from Google Cloud Storage"""
    from google.cloud import storage
    from utils import create_folder
    storage_client = storage.Client.from_service_account_json('privatekey.json')
    blobs = storage_client.list_blobs(
        "ews-data", prefix="data/"
    )
    for blob in blobs:
        print("Downloading: ", blob.name)
        create_folder(blob.name[0:blob.name.rindex("/")])
        blob.download_to_filename(blob.name)


def createTIF(date, files):
    """
    Creates a new TIF file locally using downloaded files in tmp/ folder
    OUTDATED DO NOT USE THIS!
    """
    fileName = getUniqueFile("processed/{}.tif".format(date))
    print("Creating TIF file: {}".format(fileName))

    randomBand = rasterio.open("tmp/" + files[0])
    meta = randomBand.meta
    meta.update(count=len(files))

    with rasterio.open(fileName, 'w+', **meta) as output:
        i = 0
        for name in files:
            i += 1
            tifImage = rasterio.open("tmp/{}".format(name))
            output.write_band(i, tifImage.read(1))


def main(args=None):
    """This code is used to create a combined Sentinel-1 & Sentinel-2 image by stacking their bands.
    It's experimental and not used in the thesis because there is currently no way to make sure that Sentinel-2 image is not too cloudy
    """
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument("-f", "--file",
                        required=False,
                        help="Folder to check for geojson files (end with /)",
                        default="data/")
    parser.add_argument("-b", "--bucket", default=secret.BUCKET, required=False,
                        help="Export TIF to cloud storage bucket")

    parser.add_argument("-d", "--download", default=False, required=False,
                        help="Download images after Exporting to GCS bucket")

    startDate = datetime(2016, 1, 1)
    endDate = datetime(2020, 1, 31)
    date = startDate

    while date < endDate:
        # Download for current date
        date += timedelta(weeks=26)

    options = parser.parse_args(args)

    if options.file == "data/":
        # Download every GEOJson file
        jsons = glob("{}*.geojson".format(options.file))
        tasks = []
        for json in jsons:
            file_name = os.path.basename(json).split(".")[0]
            print("Downloading images for {}\n".format(json))
            if options.bucket != None:
                regions = getRegions(json)
                for i, region in enumerate(regions):
                    boundsFilter = ee.Filter.geometry(ee.Geometry(region))
                    times = [t[:10] for t in getSentinel2DatesForRegion(region)]
                    dates = [datetime.strptime(x[0:10], "%Y-%m-%d") for x in times]
                    print("Dates found for region: {} in File: {}: {}\n".format(i, file_name, len(times)))
                    for rx, date in enumerate(dates):
                        startDate = date - timedelta(days=8)
                        endDate = date + timedelta(days=8)
                        timeFilter = ee.Filter.date(ee.Date(startDate), ee.Date(endDate))
                        image = createCombinedImage(boundsFilter, timeFilter)
                        sys.stdout.write(
                            "\rAdded {} of {} dates for File, Region: {}, {}".format(rx, len(dates), file_name, i))
                        tasks.append(exportTIF(image, region, file_name, date.strftime("%Y-%m-%d"), i))
                if options.download:
                    downloadGCS()
        while any(task.status()["state"] in ["READY", "RUNNING"] for task in tasks):
            done = len([t for t in tasks if t.status()["state"] in ["COMPLETED", "FAILED"]])
            count = len(tasks)
            sys.stdout.write("\rWaiting for tasks to complete: ({}/{})".format(done, count))
            time.sleep(5)
        print("\nTask completed")


if __name__ == '__main__':
    try:
        main()
    except MemoryError as e:
        logger.error(e)
