import os
import sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from utils import *


def _bytes_feature(value):
    """Returns a bytes_list from a string / byte."""
    # If the value is an eager tensor BytesList won't unpack a string from an EagerTensor.
    if isinstance(value, type(tf.constant(0))):
        value = value.numpy()
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _int64_feature(value):
    """Returns an int64_list from a bool / enum / int / uint."""
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _float32_feature(value):
    """Returns an int64_list from a bool / enum / int / uint."""
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))


def serialize_data(image, labels, image_shape, labels_shape):
    feature = {
        'image': _bytes_feature(image),
        'labels': _int64_feature(int(labels)) if labels_shape is None else _bytes_feature(labels),
        'width': _int64_feature(image_shape[0]),
        'height': _int64_feature(image_shape[1]),
        'n_bands': _int64_feature(image_shape[2]),
        'n_classes': _int64_feature(1 if labels_shape is None else labels_shape[2])
    }  # Create a Features message using tf.train.Example.
    features = tf.train.Example(features=tf.train.Features(feature=feature))
    return features.SerializeToString()


class Generator:

    def __init__(self, files, tfrecord_prefix, load_file, dtype):
        self.current_file = None
        self.x, self.y = None, None
        self.writer = None
        self.file_index = 0
        self.tf_prefix = tfrecord_prefix
        self.files = files
        self.load_file = load_file
        self.dtype = dtype

    def get_file(self):
        if self.file_index >= len(self.files):  # Go back to first file after reaching end of files
            self.file_index = 0
        return self.files[self.file_index]

    def get_x_y(self):
        if self.get_file() != self.current_file:  # Load x and y for new file
            self.x, self.y = self.load_file(self.get_file())

        if self.x is None or self.y is None:  # Check if file returns valid X, Y
            print("Skipping: {} cuz its borked".format(self.get_file()))
            self.file_index += 1
            return self.get_x_y()
        else:  # Save current file on correct X, Y
            self.current_file = self.get_file()
        return self.x, self.y


class ImageSegmentation(Generator):

    def __init__(self, files, tfrecord_prefix, samples, batch_sz, patch_sz=160,
                 patches_per_file=3,
                 load_file=load_numpy,
                 transform=True,
                 dtype=np.float32):
        super().__init__(files, tfrecord_prefix, load_file, dtype)
        self.total_size = samples
        self.batch_sz = batch_sz
        self.patch_sz = patch_sz
        self.transform = transform
        self.ppf = patches_per_file
        for batch_i in range(0, self.total_size, self.batch_sz):
            tfrecords_output = self.tf_prefix + "_{}-{}.tfrecords".format(batch_i, batch_i + self.batch_sz - 1)
            print("Creating TFRecord: ", tfrecords_output)
            if not os.path.exists(tfrecords_output):
                open(tfrecords_output, 'w').close()
            with tf.io.TFRecordWriter(tfrecords_output) as writer:
                for i in range(self.batch_sz):
                    if i % self.ppf == 0:  # 3 patches per files max
                        self.file_index += 1
                    print("Batch: {}, Sample: {}, File: {}".format(batch_i / self.batch_sz, i, self.get_file()))
                    self.write(writer)

    def get_rand_patch(self):
        img, mask = self.get_x_y()
        return get_rand_patch(img, mask, self.patch_sz, self.transform, self.dtype)

    def write(self, writer):
        img, y = self.get_rand_patch()
        image_bytes = tf.io.serialize_tensor(img)
        labels_bytes = tf.io.serialize_tensor(y)

        example = serialize_data(image_bytes, labels_bytes, img.shape, y.shape)
        writer.write(example)


class CenterPixelTarget(Generator):

    def __init__(self, files, tfrecord_prefix, samples, batch_sz, patch_sz=301,
                 patches_per_file=10, t_steps=0, load_file=load_numpy,
                 transform=True, dtype=np.float32):
        """
        t_steps should be higher than 0 if the label should be generated from an image later in the cycle
        """
        super().__init__(files, tfrecord_prefix, load_file, dtype)
        self.total_size = samples
        self.batch_sz = batch_sz
        self.patch_sz = patch_sz
        self.n_patches = patches_per_file
        self.t_steps = t_steps
        self.transform = transform
        for batch_i in range(0, self.total_size, self.batch_sz):
            tfrecords_output = self.tf_prefix + "_{}-{}.tfrecords".format(batch_i, batch_i + self.batch_sz - 1)
            print("Creating TFRecord: ", tfrecords_output)
            if not os.path.exists(tfrecords_output):
                open(tfrecords_output, 'w').close()
            with tf.io.TFRecordWriter(tfrecords_output) as self.writer:
                for i in range(0, self.batch_sz, self.n_patches):
                    print("Creating batch: {}, patches: {} - {}".format(batch_i / self.batch_sz, i + 1,
                                                                        i + self.n_patches))
                    self.file_index = random.randint(0, len(self.files))
                    self.create_patches()
                    self.file_index += 1

    def get_maskt1(self):
        d = get_date(self.get_file())
        region = get_file_name(self.get_file())
        dates = sorted(set(map(get_date, self.files)))
        try:
            new_date = dates[dates.index(d) + self.t_steps]
        except IndexError as e:
            print("File: {} can't have more t+{} steps, no more dates available".format(self.get_file(), self.t_steps))
            return None
        next_files = [f for f in self.files if new_date in f and region in f]
        if len(next_files) > 0:
            try:
                return getLabelsForTIF(next_files[0]).transpose([1, 2, 0])[:, :, [1]]
            except (RasterioIOError, AssertionError, TypeError) as e:
                print("Error getting labels for file: {}".format(next_files[0]))
        return None

    def create_patches(self):
        img, mask = self.get_x_y()
        if self.t_steps > 0:
            maskt1 = self.get_maskt1()
            if maskt1 is None:
                self.file_index += 1
                return self.create_patches()
            else:
                try:
                    xs, ys = get_rand_patches_1pp_t1(img, mask, maskt1,
                                                     self.patch_sz, self.n_patches, self.transform,
                                                     True,
                                                     self.dtype)
                except AssertionError as e:
                    print("File {} does not have enough new deforestation patches".format(self.get_file()))
                    self.file_index += 1
                    return self.create_patches()
        else:
            xs, ys = get_rand_patches_1pp(img, mask, self.patch_sz, self.n_patches, self.transform, True, self.dtype)
        for i in range(len(xs)):
            x = xs[i]
            y = ys[i]
            image_bytes = tf.io.serialize_tensor(x)
            data = serialize_data(image_bytes, y, x.shape, None)
            self.writer.write(data)


## END OF CLASS

def load_file(file):
    x, y = load_numpy(file)
    if x is None or y is None:
        return None, None
    x = np.concatenate((x, y[:, :, [0, 3, 4, 5]]), axis=2)  # Test with adding the context to the images
    return x, y[:, :, [1]]


def create_tfrecords(files, total_samples=1000, batch_size=100, patch_sz=160,
                     center_pixel_prediction=False,
                     patches_per_file=10,
                     t_steps=0,
                     dtype=np.float32,
                     tfrecord_dir="tfrecords/"):
    directory = os.path.dirname(tfrecord_dir)
    if not os.path.exists(directory):
        os.makedirs(directory)
    random.shuffle(files)
    split = int(len(files) / 4 * 3)
    train_files = files[0:split]
    test_files = files[split:len(files)]
    if center_pixel_prediction:
        CenterPixelTarget(train_files,
                          tfrecord_dir + "train",
                          int(total_samples / 4 * 3),
                          batch_size,
                          patches_per_file=patches_per_file,
                          t_steps=t_steps,
                          load_file=load_file,
                          dtype=dtype,
                          patch_sz=patch_sz)
        CenterPixelTarget(test_files,
                          tfrecord_dir + "test",
                          int(total_samples / 4),
                          batch_size,
                          patches_per_file=patches_per_file,
                          t_steps=t_steps,
                          load_file=load_file,
                          dtype=dtype,
                          patch_sz=patch_sz)
    else:
        ImageSegmentation(train_files,
                          tfrecord_dir + "train",
                          int(total_samples / 4 * 3),
                          batch_size,
                          load_file=load_file,
                          dtype=dtype,
                          patch_sz=patch_sz)
        ImageSegmentation(test_files,
                          tfrecord_dir + "test",
                          int(total_samples / 4),
                          batch_size,
                          load_file=load_file,
                          dtype=dtype,
                          patch_sz=patch_sz)


if __name__ == "__main__":
    files = glob("data/**/*.tif", recursive=True)


    def get_year(f):
        split = f.split("/")
        date = [part for part in split if len(part) == 10][0]
        return int(date[0:4])


    def get_location(f):
        split = f.split("/")
        return [part for part in split if len(part) == 5][0]

    files_2018_up = [f for f in files if get_year(f) >= 2018]
    year_2017_up_003_1 = [f for f in files if get_year(f) >= 2018 and get_location(f) == "003_1"]
    all_years_003_1 = [f for f in files if get_location(f) == "003_1"]

    create_tfrecords(files_2018_up, total_samples=200,
                     patch_sz=301, center_pixel_prediction=True,
                     t_steps=1,
                     patches_per_file=10, tfrecord_dir="tfrecords/center_pixel/"
                     )
